export const selectCurrentUser = (state) => state.currentUser.isAuthenticated
  ? state.currentUser.user
  : null
