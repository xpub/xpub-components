export const ucfirst = (text) => text.substr(0, 1).toUpperCase() + text.substr(1)
