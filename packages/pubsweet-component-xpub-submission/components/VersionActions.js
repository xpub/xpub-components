import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import './Actions.css'

const VersionActions = ({ project, version }) => (
  <div className="actions">
    <div className="action">
      <Link to={`/editor/${project.id}/${version.id}`} target="editor">{version.frozen ? 'view your manuscript' : 'edit your manuscript'}</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/declarations/${version.id}`}>{version.frozen ? 'view declarations' : 'edit declarations'}</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/submit-review/${version.id}`}>submit for peer review</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/submit-preprint/${version.id}`}>publish preprint</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/check/${version.id}`}>check submission</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/reviewers/${version.id}`}>invite reviewers</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/review/${version.id}`}>submit your review</Link>
    </div>

    <div className="action">
      <Link to={`/projects/${project.id}/decision/${version.id}`}>submit your decision</Link>
    </div>
  </div>
)

VersionActions.propTypes = {
  project: PropTypes.object.isRequired,
  version: PropTypes.object.isRequired
}

export default VersionActions
