import React from 'react'
import PropTypes from 'prop-types'
import RolesSummaryItemContainer from './RolesSummaryItemContainer'

const roleTypes = ['owner', 'editor', 'reviewer']

const RolesSummary = ({ project, roles = {} }) => (
  <div className="content-metadata" style={{ display: 'table', margin: 10, borderLeft: '1px solid #ddd' }}>

    {roleTypes.map(roleType => {
      const rolesofType = roles[roleType]

      if (!rolesofType) return null

      return Object.keys(rolesofType).map(id => {
        const role = rolesofType[id]

        return (
          <RolesSummaryItemContainer key={id} roleId={id} roleType={roleType} project={project} role={role}/>
        )
      })
    })}
  </div>
)

RolesSummary.propTypes = {
  project: PropTypes.object.isRequired,
  roles: PropTypes.object.isRequired
}

export default RolesSummary
